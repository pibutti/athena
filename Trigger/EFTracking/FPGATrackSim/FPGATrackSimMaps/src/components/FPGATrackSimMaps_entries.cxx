#include "../FPGATrackSimMappingSvc.h"
#include "../FPGATrackSimSpacePointsTool.h"
#include "../FPGATrackSimHitFilteringTool.h"
#include "../FPGATrackSimClusteringTool.h"
#include "../FPGATrackSimClusteringOfflineTool.h"

DECLARE_COMPONENT(FPGATrackSimMappingSvc)
DECLARE_COMPONENT( FPGATrackSimClusteringTool )
DECLARE_COMPONENT( FPGATrackSimClusteringOfflineTool )
DECLARE_COMPONENT(FPGATrackSimSpacePointsTool)
DECLARE_COMPONENT(FPGATrackSimHitFilteringTool)
DECLARE_COMPONENT(FPGATrackSimMappingSvc)
